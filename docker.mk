include .env

.PHONY: up down stop prune ps shell drush logs

default: up

DRUPAL_ROOT ?= /var/www/english-helper
PHP_CONTAINER_NAME = '$(PROJECT_NAME)_php'
DB_CONTAINER_NAME = '$(PROJECT_NAME)_mariadb'

up:
	@echo "Starting up containers for $(PROJECT_NAME)..."
	docker-compose up -d --remove-orphans

pull:
	@echo "Pulling containers for $(PROJECT_NAME)..."
	docker-compose pull --parallel
	docker-compose up -d --remove-orphans

down: stop

stop:
	@echo "Stopping containers for $(PROJECT_NAME)..."
	@docker-compose stop

prune:
	@echo "Removing containers for $(PROJECT_NAME)..."
	@docker-compose down -v

ps:
	@docker ps --filter name='$(PROJECT_NAME)*'

shell:
	docker exec -ti -e COLUMNS=$(shell tput cols) -e LINES=$(shell tput lines) $(PHP_CONTAINER_NAME) sh

db_shell:
	docker exec -ti -w /var/www/backups -e COLUMNS=$(shell tput cols) -e LINES=$(shell tput lines) $(DB_CONTAINER_NAME) bash

drush:
	docker exec $(PHP_CONTAINER_NAME) drush -r $(DRUPAL_ROOT) $(filter-out $@,$(MAKECMDGOALS))

composer:
	docker exec $(PHP_CONTAINER_NAME) composer $(filter-out $@,$(MAKECMDGOALS))

logs:
	@docker-compose logs -f $(filter-out $@,$(MAKECMDGOALS))

dev_pull:
	git checkout integration
	git pull upstream integration --rebase
	make composer install
	make drush config-import
	make drush updb
	make drush entup
	make drush cache:rebuild

# https://stackoverflow.com/a/6273809/1826109
%:
	@:

