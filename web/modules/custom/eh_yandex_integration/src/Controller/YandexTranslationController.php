<?php

namespace Drupal\eh_yandex_integration\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Yandex translation callbacks controllers.
 */
class YandexTranslationController extends ControllerBase {

  /**
   * Page callback method for '/ajax/yandex_translation_api' path.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Data to be handle by js script.
   */
  public function translateCallback() {
    if (!empty($_POST['phrase'])) {
      $phrase = $_POST['phrase'];
    }
    $data['translation'] = $phrase;
    $response = new JsonResponse();
    $response->setData($data);
    return $response;
  }

}
