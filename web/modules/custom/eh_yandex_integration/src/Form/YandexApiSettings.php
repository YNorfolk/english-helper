<?php

namespace Drupal\eh_yandex_integration\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Configure Yandex translation API settings form.
 */
class YandexApiSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'yandex_api_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'eh_yandex_integration.yandex_api',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('eh_yandex_integration.yandex_api');
    $desc_key_uri = Url::fromUri('https://tech.yandex.ru/keys/', ['attributes' => ['target' => '_blank']]);
    $desc_link = Link::fromTextAndUrl($this->t('Click to get more info'), $desc_key_uri);

    $form['yandex_api_dict_key'] = array(
      '#type' => 'textfield',
      '#description' => $desc_link,
      '#title' => $this->t('Yandex translation API key'),
      '#default_value' => $config->get('dict_key'),
    );

    $desc_api_info_url = Url::fromUri('https://tech.yandex.ru/dictionary/doc/dg/reference/lookup-docpage/', ['attributes' => ['target' => '_blank']]);
    $desc_link->setUrl($desc_api_info_url);

    $form['yandex_api_dict_url'] = array(
      '#type' => 'textfield',
      '#description' => $desc_link,
      '#title' => $this->t('Yandex translation API url'),
      '#default_value' => $config->get('dict_url'),
    );
    $form['yandex_api_dict_lang'] = array(
      '#type' => 'textfield',
      '#description' => $desc_link,
      '#title' => $this->t('Yandex translation API language'),
      '#default_value' => $config->get('dict_lang'),
    );
    $form['yandex_api_dict_ui'] = array(
      '#type' => 'textfield',
      '#description' => $desc_link,
      '#title' => $this->t('Yandex translation API UI'),
      '#default_value' => $config->get('dict_ui'),
    );
    $form['yandex_api_dict_flags'] = array(
      '#type' => 'textfield',
      '#description' => $desc_link,
      '#title' => $this->t('Yandex translation API flags'),
      '#default_value' => $config->get('dict_flags'),
    );

    $licence_info_url = Url::fromUri('https://tech.yandex.ru/dictionary/doc/dg/concepts/design-requirements-docpage/', ['attributes' => ['target' => '_blank']]);
    $licence_info_link = Link::fromTextAndUrl($this->t('Click to get more info'), $licence_info_url);

    $form['yandex_api_licence_link'] = array(
      '#type' => 'textfield',
      '#description' => $licence_info_link,
      '#title' => $this->t('Yandex translation API licence link url'),
      '#default_value' => $config->get('dict_licence_url'),
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('eh_yandex_integration.yandex_api')
      ->set('dict_key', $form_state->getValue('yandex_api_dict_key'))
      ->set('dict_url', $form_state->getValue('yandex_api_dict_url'))
      ->set('dict_lang', $form_state->getValue('yandex_api_dict_lang'))
      ->set('dict_ui', $form_state->getValue('yandex_api_dict_ui'))
      ->set('dict_flags', $form_state->getValue('yandex_api_dict_flags'))
      ->set('dict_licence_url', $form_state->getValue('yandex_api_licence_link'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
