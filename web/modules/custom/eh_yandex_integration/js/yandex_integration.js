(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.ajaxTranslationRequest = {
    attach: function (context, settings) {
      $(context).find('textarea.translation-input').once().change(function () {
        var $this = $(this);
        var settings = drupalSettings.eh_yandex_integration;
        var url = settings.dict_url;
        var key = settings.dict_key;
        var lang = settings.dict_lang;
        var ui = settings.dict_ui;
        var flag = settings.dict_flags;
        var text = $this.val();
        var path = url + '?key=' + key + '&text=' + text + '&lang=' + lang + '&ui=' + ui + '&flags=' + flag;

        $.ajax({
          url: path,
          type: 'POST',
          data: {},
          success: function (data) {
            if (data.def) {
              printResults(data);
            }
          }
        });
      });
    }
  };

  Drupal.behaviors.toggleExamples = {
    attach: function (context, settings) {
      $(context).find('span.toggle').once('toggle-examples-once').click(function () {
        if (this.textContent == this.dataset.HideText){
          $.each($('ul.dictionary-examples'), function() {
            $(this).hide();
          });
          this.textContent = this.dataset.ShowText;
        }
        else {
          $.each($('ul.dictionary-examples'), function() {
            $(this).show();
          });
          this.textContent = this.dataset.HideText;
        }
      });
    }
  };

  Drupal.behaviors.autoCompleteTranslation = {
    attach: function (context, settings) {
      $(context).find('.field--name-title input').once('toggle-examples-once').change(function () {
        var $text = $(this).val();
        var $translation_input = $(context).find('textarea.translation-input');
        $translation_input.text($text);
        $translation_input.trigger('change');
      });
    }
  };

  function printResults(json) {
    var is_examples = false;
    var is_trancrip_inserted = false;
    var ul = document.createElement('ul');
    ul.className = 'tab-content';

    json.def.forEach(function (value, index) {
      var li = document.createElement('li');
      li.className = 'dictionary-item';
      var div_title = document.createElement('div');
      div_title.className = 'dictionary-title';

      var span = document.createElement('span');
      span.className = 'dictionary-original';
      span.textContent = value.text;
      div_title.appendChild(span);

      var span = document.createElement('span');
      span.className = 'dictionary-transcription';
      span.textContent = ' [' + value.ts + '] ';
      div_title.appendChild(span);

      // Put transcription to transcription field.
      if (!is_trancrip_inserted) {
        is_trancrip_inserted = true;
        insertTranscription(value.ts);
      }

      var span = document.createElement('span');
      span.className = 'dictionary-pos';
      span.textContent = value.pos;
      div_title.appendChild(span);

      li.appendChild(div_title);

      var ol = document.createElement('ol');
      ol.className = 'dictionary-meaning';

      if (value.tr) {
        value.tr.forEach(function (meaning) {
          var li = document.createElement('li');
          var div = document.createElement('div');
          div.className = 'dictionary-meaning-value';

          var span = document.createElement('span');
          span.className = 'dictionary-meaning';
          span.textContent = meaning.text;
          div.appendChild(span);

          if (meaning.gen) {
            var span = document.createElement('span');
            span.className = 'dictionary-mark';
            span.textContent = ' ' + meaning.gen;
            div.appendChild(span);
          }

          li.appendChild(div);

          if (meaning.mean) {
            var div_synonym = document.createElement('div');
            div_synonym.className = 'dictionary-synonym';
            div_synonym.textContent = '(';

            meaning.mean.forEach(function (synonym, index, array) {
              var span_synonym = document.createElement('span');
              span_synonym.textContent = synonym.text;
              div_synonym.appendChild(span_synonym);

              if ((index + 1) != array.length) {
                div_synonym.textContent += ', ';
              }
            });

            div_synonym.textContent += ')';
            li.appendChild(div_synonym);
          }

          if (meaning.ex) {
            is_examples = true;
            var ul_examples = document.createElement('ul');
            ul_examples.className = 'dictionary-examples';

            meaning.ex.forEach(function (example) {
              var li_example = document.createElement('li');
              li_example.textContent = example.text + ' - ' + example.tr[0].text;
              ul_examples.appendChild(li_example);
            });

            li.appendChild(ul_examples);
          }

          ol.appendChild(li);
        });
      }

      li.appendChild(ol);
      ul.appendChild(li);
    });

    if (is_examples) {
      var toggle = document.createElement('span');
      toggle.className = 'toggle';
      toggle.textContent = 'Hide examples';
      toggle.dataset.ShowText = 'Show examples';
      toggle.dataset.HideText = 'Hide examples';
      ul.querySelector('div.dictionary-title').appendChild(toggle);
    }

    var node = document.querySelector('div.translation-output');
    while (node.firstChild) {
      node.removeChild(node.firstChild);
    }
    document.querySelector('div.translation-output').appendChild(ul);
    Drupal.attachBehaviors();
  }

  function insertTranscription(trancrip) {
    var $iframe = $('div.field--name-field-translation-transcription iframe');
    $iframe.ready(function() {
      $iframe.contents().find("body p").text('[' + trancrip + ']');
    });
  }
})(jQuery, Drupal, drupalSettings);