(function ($, Drupal) {
    Drupal.behaviors.ajaxTranslationRequest = {
        attach: function (context, settings) {
            $(context).find('div.block-title').once().click(function () {
                var $table = $(this.nextElementSibling);
                if ($table.hasClass('table-hide')) {
                    $table.removeClass('table-hide');
                } else {
                    $table.addClass('table-hide');
                }
            });
        }
    };
})(jQuery, Drupal);