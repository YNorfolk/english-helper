<?php

namespace Drupal\eh_site\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'English Helper' Block.
 *
 * @Block(
 *   id = "english_helper_block",
 *   admin_label = @Translation("English helper block"),
 * )
 */
class EnglishHelperBlock extends BlockBase {

  /**
   * User personalized cache tag for eh_list_translation.random_list view.
   */
  const RANDOM_ITEMS_CACHE_TAG = 'view_tag.random_list.uid:';

  /**
   * User personalized cache tag for eh_list_translation.random_list view.
   */
  const RANDOM_ITEMS_CACHE_KEY = 'view_key.random_list.uid:';

  /**
   * Time threshold for new translations.
   */
  const NEW_ITEMS_TIME_DIFF = 60 * 60 * 24 * 3;

  /**
   * {@inheritdoc}
   */
  public function build() {
    $displays = [
      'word_list' => 'Word',
      'phrasal_verb_list' => 'Phrasal verb',
      'technical_list' => 'Technical',
      'useful_phrase_list' => 'Useful phrase',
      'random_list' => 'random_list',
    ];
    $items = [];

    foreach ($displays as $display => $term_name) {
      $items[$display]['view_results'] = $this->getViewResults('eh_list_translation', $display);

      if ($display != 'random_list') {
        $items[$display]['count'] = $this->getNumberTranslations($term_name);
      }
    }

    return [
      '#theme' => 'eh_site_english_helper_block',
      '#items' => $items,
      '#attached' => [
        'library' => [
          'eh_site/eh_site',
        ],
      ],
    ];
  }

  /**
   * Return rendered views results.
   *
   * @return string
   *   Data to be rendered.
   */
  private function getViewResults($view_name, $display = 'default', $args = '') {
    if ($display != 'random_list') {
      $views_results = views_embed_view($view_name, $display, $args);
      $results = \Drupal::service('renderer')->render($views_results);
    }
    else {
      $uid = \Drupal::currentUser()->id();
      $cache_key = self::RANDOM_ITEMS_CACHE_KEY . $uid;
      $cache_result = \Drupal::cache('render')->get($cache_key);

      if (empty($cache_result->data)) {
        $views_results = views_embed_view($view_name, $display, $args);
        $results = \Drupal::service('renderer')->render($views_results);
        self::setRandomItemsCache($results, $uid);
      }
      else {
        $results = $cache_result->data;
      }
    }

    return $results;
  }

  /**
   * Helper function to put into cache rendered random_items view results.
   */
  public static function setRandomItemsCache($data, $uid) {
    if (!empty($data) && !empty($uid)) {
      $expire_time = \Drupal::time()->getCurrentTime() + 60 * 60 * 24;
      $cache_tag = self::RANDOM_ITEMS_CACHE_TAG . $uid;
      $cache_key = self::RANDOM_ITEMS_CACHE_KEY . $uid;
      \Drupal::cache('render')->set($cache_key, $data, $expire_time, array($cache_tag));
    }
  }

  /**
   * Helper to get translations count with passed vocabulary for current user.
   *
   * @param string $term_name
   *   Taxonomy 'vocabulary' term name.
   *
   * @return string
   *   String with translations count for rendering or empty string.
   */
  private function getNumberTranslations($term_name) {
    $uid = \Drupal::currentUser()->id();
    $query = \Drupal::entityQuery('node')
      ->condition('status', 1)
      ->condition('uid.entity.uid', $uid)
      ->condition('field_translation_vocabulary.entity.name', $term_name)
      ->count();

    $count = clone $query;
    $count = $count->execute();

    if ($count) {
      $result = "($count " . $this->formatPlural($count, 'item', 'items total');

      $time_diff = \Drupal::time()->getRequestTime() - self::NEW_ITEMS_TIME_DIFF;
      $new_count = $query->condition('changed', $time_diff, '>')
        ->execute();

      if ($new_count) {
        $result .= " and $new_count new";
      }

      $result .= ')';
      return $result;
    }

    return '';
  }

}
