<?php

namespace Drupal\eh_site\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\views\Views;
use Drupal\Core\Cache\Cache;
use Drupal\eh_site\Plugin\Block\EnglishHelperBlock;

/**
 * Eh ajax callback controller.
 *
 * @ingroup routing
 */
class EhAjaxController extends ControllerBase {

  /**
   * Page callback method for '/ajax/refresh_random_items' path.
   */
  public function refreshRandomItems() {
    $response = new AjaxResponse();
    $view = Views::getView('eh_list_translation');

    if (!empty($view)) {
      $view->setDisplay('random_list');

      // Refresh cache for random_items.
      // @see  EnglishHelperBlock::getViewResults() method.
      $uid = \Drupal::currentUser()->id();
      $cache_tag = EnglishHelperBlock::RANDOM_ITEMS_CACHE_TAG . $uid;
      Cache::invalidateTags([$cache_tag]);

      $result = $view->preview();

      if (!empty($result)) {
        $html = \Drupal::service('renderer')->render($result);
        EnglishHelperBlock::setRandomItemsCache($html, $uid);
        $selector = '.random-items-table';
        $response->addCommand(new HtmlCommand($selector, $html));
      }
    }

    return $response;
  }

}
